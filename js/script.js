var programy;
var cetnosti;

function onlyUnique(value, index, self) {
	return self.indexOf(value) === index;
}

function showProgram(strana, tema) {

	$("#tabulka").empty();

	var text = '<h2>' + strana + ', ' + tema + '</h2>';


	var body = programy.filter(function(d) {
		return (d.strana == strana && (d.tag1 == tema || d.tag2 == tema || d.tag3 == tema || d.tag4 == tema || d.tag5 == tema || d.tag6 == tema || d.tag7 == tema));
	});

	tagy = body.map(function(d) {
		var tagysmrizkou = [];
		tagysmrizkou.push('#' + d.tag1);
		if (d.tag2 != '') {
			tagysmrizkou.push(' #' + d.tag2);
			if (d.tag3 != '') {
				tagysmrizkou.push(' #' + d.tag3);
				if (d.tag4 != '') {
					tagysmrizkou.push(' #' + d.tag4);
					if (d.tag5 != '') {
						tagysmrizkou.push(' #' + d.tag5);
						if (d.tag6 != '') {
							tagysmrizkou.push(' #' + d.tag6);
							if (d.tag7 != '') {
								tagysmrizkou.push(' #' + d.tag7);
		}}}}}}
		return tagysmrizkou;
	});

	body = body.map(function(d) {
		return d.bod;
	});

	text += "<div><ul>" // + body.join("</li><li>") + "</li></ul></div>";

	for (i = 0; i < body.length; i++) {
		text += '<li>'
		text += body[i]
		text += '<div class="tagy"><span>' + tagy[i].join("</span><span>") + '</span></div>'
		text += '</li>'
	}

	text += '</ul></div>'

	text += '<div class="zpet"><a href="#programy" class="js-tabulka">Zpět k tabulce</a></div>';

	$("#vypis").html(text);


};

function showTablicu() {


		$("#vypis").empty();

		var fotka = ["ano.jpg", "čps.jpg", "čssd.jpg", "kdu.jpg", "ksčm.jpg", "ods.jpg", "spd.jpg", "stan.jpg", "top.jpg"];



		// záhlaví

		var strany = cetnosti.map(function(d) {
						return d.strana;
					});

		strany = strany.filter(onlyUnique);

		var text = '<tr class="loga"><td><div class="pole"></div></td>';

		for (i = 0; i < strany.length; i++) {

			text += '<td><div class="pole"><strong><img src="https://interaktivni.rozhlas.cz/data/programy-stran-2017/charts/loga/' + fotka[i] + '" width=100%></strong></div></td>';

		}

		text += '</tr>';



		// řádky

		var temata = cetnosti.map(function(d) {
						return d.tema;
					});

		temata = temata.filter(onlyUnique);

		var obsah = cetnosti.map(function(d) {
						return d.obsah;
					});

		for (j = 0; j < temata.length; j++) {

			var tooltip = obsah[j];

			var rozdeleni = temata[j];
			if (rozdeleni == 'Bezpečnost') {
				rozdeleni = 'Bezpeč-<br>nost';
			} else if (rozdeleni == 'Ekonomika') {
				rozdeleni = 'Ekono-<br>mika';
			} else if (rozdeleni == 'Informatika') {
				rozdeleni = 'Informa-<br>tika';
			} else if (rozdeleni == 'Zahraničí') {
				rozdeleni = 'Zahra-<br>ničí';
			} else if (rozdeleni == 'Zdravotnictví') {
				rozdeleni = 'Zdravot-<br>nictví';
			} else if (rozdeleni == 'Zemědělství') {
				rozdeleni = 'Zeměděl-<br>ství';
			}

			text += '<tr class="program-row"><th>' + rozdeleni + '<div class="tooltip"><span class="tooltiptext">' + tooltip + '</span></div></th>';

			cetnosti_ = cetnosti.filter(function(d) {
					return (d.tema == temata[j]);
			});

			for (i = 0; i < strany.length; i++) {

				var podil = Math.round(cetnosti_[i].freq * 1000, 3)/10;

				var odstin;
				if (cetnosti_[i].freq < 0.01) {
					odstin = "#fee5d9";
				} else if (cetnosti_[i].freq < 0.02) {
					odstin = "#fcbba1";
				} else if (cetnosti_[i].freq < 0.04) {
					odstin = "#fc9272";
				} else if (cetnosti_[i].freq < 0.08) {
					odstin = "#fb6a4a";
				} else if (cetnosti_[i].freq < 0.16) {
					odstin = "#de2d26";
				} else {
					odstin = "#a50f15";
				}

				var tooltip = programy.filter(function(d) {
					return (d.strana == strany[i] && d.top == temata[j] && (d.tag1 == temata[j] || d.tag2 == temata[j] || d.tag3 == temata[j] || d.tag4 == temata[j] || d.tag5 == temata[j] || d.tag6 == temata[j] || d.tag7 == temata[j]));
				})

				tooltip = tooltip.map(function(d) {
					return d.bod;
				})

				tooltip = "<ul><li>" + tooltip.join("</li><li>") + "</li></ul>";

				text += '<td class="n-tooltip" data-tooltip-content="#tooltip_content-'+i+'-'+j+'" style="background-color: ' +
					odstin + '; color: white;"><div class="pole">' +
					podil + '&nbsp;%</div><div class="tooltip_templates"><span id="tooltip_content-'+i+'-'+j+'">'+tooltip+' <div class="alles"><a href="#programy" data-strana="' + strany[i] + '" data-tema=' + temata[j] + ' class="js-program">Všechny body</a></div></td>';
					 
			}

			text += '</tr>';

		}



		// vykreslení tabulky

		$("#tabulka").html(text);
		$('.n-tooltip').tooltipster({
			maxWidth: 200,
			contentAsHTML: true,
		interactive: true
		});

};




d3.select("#fallback").remove();



d3.csv("https://interaktivni.rozhlas.cz/data/programy-stran-2017/charts/programy.csv", function(data1) {
	d3.csv("https://interaktivni.rozhlas.cz/data/programy-stran-2017/charts/cetnosti.csv", function(data2) {

		programy = data1;
		cetnosti = data2;



		showTablicu();

	})
});

$("body").on("click", ".js-tabulka", function() {
	showTablicu();

});

$("body").on("click", ".js-program", function() {
	showProgram($(this).data("strana"), $(this).data("tema"))
});


